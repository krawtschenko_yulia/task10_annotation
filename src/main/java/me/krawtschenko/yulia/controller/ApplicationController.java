package me.krawtschenko.yulia.controller;

import me.krawtschenko.yulia.model.BusinessLogic;
import me.krawtschenko.yulia.view.ApplicationView;
import me.krawtschenko.yulia.view.MenuAction;
import me.krawtschenko.yulia.view.MenuItem;
import me.krawtschenko.yulia.view.MenuOption;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ApplicationController {
    private BusinessLogic model;
    private ApplicationView view;

    private MenuAction getAnnotatedFields;
    private MenuAction getAnnotations;
    private MenuAction useUnknownMethods;
    private MenuAction setValueOfUnknownField;
    private MenuAction invokeVarargsMethods;
    private MenuAction decipherClass;

    private final Logger logger = LogManager.getLogger(ApplicationController.class);

    public ApplicationController() {
        try {
            model = new BusinessLogic();
            view = new ApplicationView();

            getAnnotatedFields = new MenuAction() {
                @Override
                public void execute() {
                    try {
                        view.printMessage("Fields of Page class with" +
                                " HtmlAttribute annotation:");
                        view.print(model.getAnnotatedFields());
                    } catch (Exception ex) {
                        logger.error(ex);
                    }
                }
            };
            view.addMenuItemAction(MenuOption.GET_ANNOTATED_FIELDS,
                    getAnnotatedFields);

            getAnnotations = new MenuAction() {
                @Override
                public void execute() {
                    try {
                        view.printMessage("Page class annotations:");
                        view.print(model.getAnnotations());
                    } catch (Exception ex) {
                        logger.error(ex);
                    }
                }
            };
            view.addMenuItemAction(MenuOption.GET_ANNOTATIONS,
                    getAnnotations);

            useUnknownMethods = new MenuAction() {
                @Override
                public void execute() {
                    try {
                        view.printMessage("Returns of Page methods calls:");
                    } catch (Exception ex) {
                        logger.error(ex);
                    }
                }
            };
            view.addMenuItemAction(MenuOption.USE_UNKNOWN_METHODS,
                    useUnknownMethods);

            setValueOfUnknownField = new MenuAction() {
                @Override
                public void execute() {
                    try {
                        model.setValueOfUnknownField();
                        view.printMessage("Value of a Page class field was set" +
                                " to null.");
                    } catch (Exception ex) {
                        logger.error(ex);
                    }
                }
            };
            view.addMenuItemAction(MenuOption.SET_VALUE_OF_UNKNOWN_FIELD,
                    setValueOfUnknownField);

            invokeVarargsMethods = new MenuAction() {
                @Override
                public void execute() {
                    try {
                        String[] concatenations
                                = model.invokeConcatenatingMethods();
                        view.printMessage("Results of Varargs method calls:");
                        for (String string : concatenations) {
                            view.print(string);
                        }
                    } catch (Exception ex) {
                        logger.error(ex);
                    }
                }
            };
            view.addMenuItemAction(MenuOption.INVOKE_CONCATENATING_METHODS,
                    invokeVarargsMethods);

            decipherClass = new MenuAction() {
                @Override
                public void execute() {
                    try {
                        BusinessLogic item = new BusinessLogic();
                        view.print(model.decipherObjectClass(item));
                    } catch (Exception ex) {
                        logger.error(ex);
                    }
                }
            };
            view.addMenuItemAction(MenuOption.DECIPHER_CLASS,
                    decipherClass);

            MenuAction quit = () -> System.exit(0);
            view.addMenuItemAction(MenuOption.QUIT, quit);
        } catch (RuntimeException ex) {
            logger.error(ex.getStackTrace());
        }
    }

    public void startMenu() {
        try {
            view.displayMenu();
        } catch (RuntimeException ex) {
            logger.error(ex.getStackTrace());
        }
    }

    private String formatObjectAndTypeString(Object object) {
        return String.format("%s (%s)", object.toString(),
                object.getClass().getName());
    }
}