package me.krawtschenko.yulia.model;

@HtmlSerializable
@HtmlElement(tag="body")
public class Page {

    @HtmlAttribute(name="class")
    private String className = "full-width";

    @HtmlAttribute(name="font-family")
    private String fontFamily = "sans-serif";

    @HtmlAttribute(name="font-size")
    private double fontSize = 12.0;

    @HtmlActionListener(name="onbeforeprint")
    private void onBeforePrint() {
        fontFamily = "serif";
    }

    public double getFontSize() {
        return fontSize;
    }

    protected void setFontSize(double size) {
        fontSize = size;
    }

    public String getFontFamily() {
        return fontFamily;
    }

    private void setFontFamily(String font) {
        fontFamily = font;
    }
}
