package me.krawtschenko.yulia.model;

public class IncognitoClass {
    private Page classified;

    public IncognitoClass(Page info) {
        classified = info;
    }

    public String getCategoryString(String name, int... values) {
        StringBuilder stringToBe = new StringBuilder();

        stringToBe.append(name);
        stringToBe.append(": ");
        for(int value : values) {
            stringToBe.append(' ');
            stringToBe.append(value);
        }
        return stringToBe.toString();
    }

    public String getConcatenated(String... strings) {
        StringBuilder stringToBe = new StringBuilder();

        for(String string : strings) {
            stringToBe.append(string);
            stringToBe.append(' ');
        }
        return stringToBe.toString();
    }

    public Page getClassified() {
        return classified;
    }
}
