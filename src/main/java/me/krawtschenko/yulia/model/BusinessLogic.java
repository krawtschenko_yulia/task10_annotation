package me.krawtschenko.yulia.model;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;

public class BusinessLogic {
    public ArrayList<Field> getAnnotatedFields() {
        Class clazz = Page.class;
        Field[] declared = clazz.getDeclaredFields();
        ArrayList<Field> annotated = new ArrayList<Field>();

        for (Field field : declared) {
            if (field.getAnnotation(HtmlAttribute.class) != null) {
                annotated.add(field);
            }
        }
        return annotated;
    }

    public ArrayList<String> getAnnotations() {
        ArrayList<String> annotationTypes = new ArrayList<>();

        AnnotatedElement annotated = Page.class;
        Annotation[] annotations = annotated.getAnnotations();
        for (Annotation annotation : annotations) {
            annotationTypes.add(annotation.annotationType().getSimpleName());
        }

        return annotationTypes;
    }

    public ArrayList useUnknownMethods() throws NoSuchMethodException,
            IllegalAccessException, InvocationTargetException,
            InstantiationException {
        Class clazz = Page.class;
        Method[] methods = clazz.getDeclaredMethods();
        ArrayList returns = new ArrayList();

        for (Method method : methods) {
            method.setAccessible(true);
            Class[] parameterTypes = method.getParameterTypes();
            if (parameterTypes.length == 1) {
                Object parameter = parameterTypes[0].getConstructor()
                        .newInstance();
                if (parameter instanceof String) {
                    method.invoke(clazz,(Object) "monospace");
                } else {
                    method.invoke(clazz,12.5);
                }
            }
            else if (parameterTypes.length == 0) {
                Object returnValue = method.invoke(clazz);

                returns.add(returnValue);
            }
        }
        return returns;
    }

    public void setValueOfUnknownField() throws NoSuchFieldException, NoSuchMethodException,
            IllegalAccessException, InvocationTargetException,
            InstantiationException {
        Class clazz = Page.class;
        Object pageInstance = clazz.getConstructor().newInstance();

        Field field = clazz.getDeclaredField("className");
        field.setAccessible(true);
        field.set(pageInstance, null);
    }

    public String[] invokeConcatenatingMethods() throws NoSuchMethodException,
            IllegalAccessException, InvocationTargetException,
            InstantiationException {
        String[] concatenations = new String[2];

        Class clazz = IncognitoClass.class;

        Method[] methods = clazz.getDeclaredMethods();

        Object unknown = clazz.getConstructor().newInstance();
        Method getCategoryString = clazz
                .getDeclaredMethod("getCategoryString",
                String.class, int[].class);
        String primeLabel = "Prime numbers";
        int[] primes = new int[]{2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37};
        concatenations[0] = getCategoryString
                .invoke(unknown, primeLabel, primes)
                .toString();

        Method getConcatenated = clazz
                .getDeclaredMethod("getConcatenated", String[].class);
        String[] toBeConcatenated = { "This", "should", "be", "concatenated." };
        concatenations[1] = getConcatenated
                .invoke(unknown, new Object[]{ toBeConcatenated })
                .toString();

        return concatenations;
    }

    public ArrayList<String> decipherObjectClass(Object unknown) {
        ArrayList<String> info = new ArrayList<>();
        Class clazz = unknown.getClass();

        info.add("Descriptor: " + clazz.descriptorString());
        info.add("Name: " + clazz.getCanonicalName());
        info.add("Parent: " + clazz.getSuperclass());
        info.add("Package: " + clazz.getPackage());
        info.add("Annotations: " + Arrays.toString(clazz.getAnnotations()));
        info.add("Constructors: "
                + Arrays.toString(clazz.getDeclaredConstructors()));
        info.add("Fields: " + Arrays.toString(clazz.getDeclaredFields()));
        info.add("Methods: " + Arrays.toString(clazz.getDeclaredMethods()));
        info.add("Inner classes: "
                + Arrays.toString(clazz.getDeclaredClasses()));

        return info;
    }
}
