package me.krawtschenko.yulia.view;

import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class ApplicationView<T> extends View{
    public ApplicationView() {
        super();

        MenuItem quit = new MenuItem(MenuOption.QUIT, "Quit");
        menu.put(MenuOption.QUIT, quit);

        MenuItem getAnnotatedFields
                = new MenuItem(MenuOption.GET_ANNOTATED_FIELDS,
                "Get annotated fields from a class");
        menu.put(MenuOption.GET_ANNOTATED_FIELDS, getAnnotatedFields);

        MenuItem getAnnotations
                = new MenuItem(MenuOption.GET_ANNOTATIONS,
                "Get annotations of a class");
        menu.put(MenuOption.GET_ANNOTATIONS, getAnnotations);

        MenuItem useUnknownMethods
                = new MenuItem(MenuOption.USE_UNKNOWN_METHODS,
                "Use unknown methods of a class");
        menu.put(MenuOption.USE_UNKNOWN_METHODS, useUnknownMethods);

        MenuItem setValueOfUnknownField
                = new MenuItem(MenuOption.SET_VALUE_OF_UNKNOWN_FIELD,
                "Set value of an unknown field in a class");
        menu.put(MenuOption.SET_VALUE_OF_UNKNOWN_FIELD, setValueOfUnknownField);

        MenuItem invokeVarargsMethods
                = new MenuItem(MenuOption.INVOKE_CONCATENATING_METHODS,
                "Invoke methods with variadic number of arguments");
        menu.put(MenuOption.INVOKE_CONCATENATING_METHODS,invokeVarargsMethods);

        MenuItem decipherClass
                = new MenuItem(MenuOption.DECIPHER_CLASS,
                "Get information about an unknown object's class");
        menu.put(MenuOption.DECIPHER_CLASS, decipherClass);
    }

    public int readInt() {
        return input.nextInt();
    }

    public double readDouble() {
        return input.nextDouble();
    }

    public void printMap(Map<?, ?> map) {
        map.forEach((key, value) -> {
            System.out.println(key + ": " + value);
        });
    }

    public void printSet(Set<?> set) {
        set.forEach(System.out::println);
    }

    public void printArray(T[] array) {
        for (T element : array) {
            System.out.println(element);
        }
    }

    public void printGroupedWords(Map<?, Set<?>> map) {
        map.forEach(((key, valueSet) -> {
            System.out.print("\t");
            valueSet.forEach(System.out::println);
        }));
    }

    @Override
    public void displayMenu() {
        super.printMessage("MENU");
        super.displayMenu();
    }

    private boolean isValidLocale(String string) {
        Locale[] locales = Locale.getAvailableLocales();
        for (Locale locale : locales) {
            if (string.equals(locale.toString())) {
                return true;
            }
        }
        return false;
    }
}
