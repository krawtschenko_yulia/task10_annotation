package me.krawtschenko.yulia.view;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public enum MenuOption {
    QUIT("Q"),
    GET_ANNOTATED_FIELDS("1"),
    GET_ANNOTATIONS("2"),
    USE_UNKNOWN_METHODS("3"),
    SET_VALUE_OF_UNKNOWN_FIELD("4"),
    INVOKE_CONCATENATING_METHODS("5"),
    DECIPHER_CLASS("6");

    private final String key;

    MenuOption(String key) {
        this.key = key;
    }

    MenuOption() {
        key = "";
    }

    public String getKeyToPress() {
        return key;
    }

    private static final Map<String, MenuOption> table = new HashMap<>();

    static {
        for (MenuOption option : MenuOption.values()) {
            table.put(option.getKeyToPress(), option);
        }
    }

    public static MenuOption get(String key) {
        return table.get(key);
    }
}

class OptionKeyComparator implements Comparator<MenuOption> {
    @Override
    public int compare(MenuOption o1, MenuOption o2) {
        return o1.getKeyToPress().compareTo(o2.getKeyToPress());
    }
}
